package homework3;

import java.util.Scanner;

public class TaskPlanner {
    public static void main(String[] args) {
        String[][] schedule = new String[7][2];
        Scanner sc = new Scanner(System.in);

//        Заповнюємо розклад
        arrangeSchedule(schedule);
//        Показуємо розклад
//        displaySchedule(schedule);

        while (true){
            System.out.println("\nEnter a day of a week please (or print `exit` to exit)...");
//            За допомогою методу getAndValidateUserInput
//            отримуємо від користувача input та, перевіривши його на
//            валідність, робимо return значення дня тижня
            String userDay = getAndValidateUserInput(sc);

            showTasksForDay(userDay, schedule);

            if(userDay.equals("exit")) {
                break;
            }

        }

        sc.close();
    }

    static void arrangeSchedule(String[][] schedule){
        schedule[0][0] = "Monday   ";
        schedule[0][1] = "do homework";
        schedule[1][0] = "Tuesday  ";
        schedule[1][1] = "read 100 pages of Nietzsche";
        schedule[2][0] = "Wednesday";
        schedule[2][1] = "learn C++; go to the gym";
        schedule[3][0] = "Thursday ";
        schedule[3][1] = "meet some friends";
        schedule[4][0] = "Friday   ";
        schedule[4][1] = "go to the uncle`s birthday party";
        schedule[5][0] = "Saturday ";
        schedule[5][1] = "learn cryptotrading; visit grandparents";
        schedule[6][0] = "Sunday   ";
        schedule[6][1] = "watch a film; go to courses";
    }

    static void displaySchedule(String[][] schedule){
        for (String[] strings : schedule) {
            for (int y = 0; y < strings.length; y++) {
                if (y == 0) {
                    System.out.printf("%s — ", strings[y]);
                } else {
                    System.out.print(strings[y]);
                }
            }
            System.out.println();
        }
    }

    static String getAndValidateUserInput(Scanner sc){
        String userInput;
        String[] possibleValues = {"exit", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"};
//        За замовчуванням пишемо, що userInputIsValid = false
        boolean userInputIsValid = false;

        while(true){
            userInput = sc.nextLine().toLowerCase().replaceAll(" ","");

            //        В циклі, як тільки елемент масиву дорівнюватиме введеному
            //        користувачем значенню, змінюємо userInputIsValid на true
            //        та виходимо з циклу for
            for(String day : possibleValues){
                if(day.equals(userInput)) {
                    userInputIsValid = true;
                    break;
                }
            }

            if(!userInputIsValid){
                System.out.println("Sorry, I don't understand you, please try again...");
            }
//            Якщо userInputIsValid == true, то виходимо з циклу while
            else{
                break;
            }
        }

        return userInput;
    }

    static void showTasksForDay(String userDay, String[][] schedule){
        switch (userDay) {
            case "monday" -> printSingleTask(0, schedule);
            case "tuesday" -> printSingleTask(1, schedule);
            case "wednesday" -> printSingleTask(2, schedule);
            case "thursday" -> printSingleTask(3, schedule);
            case "friday" -> printSingleTask(4, schedule);
            case "saturday" -> printSingleTask(5, schedule);
            case "sunday" -> printSingleTask(6, schedule);
            case "exit" -> System.out.println("\nExiting program...");
        }
    }

    static void printSingleTask(int dayIndex, String[][] schedule){
        System.out.printf("Your tasks for %s: %s.\n", schedule[dayIndex][0].replaceAll(" ", ""), schedule[dayIndex][1]);
    }
}