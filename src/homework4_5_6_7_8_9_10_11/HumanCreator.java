package homework4_5_6_7_8_9_10_11;

import java.text.ParseException;

public interface HumanCreator {
    public Human bornChild() throws ParseException;
}
