package homework4_5_6_7_8_9_10_11;

import java.util.Objects;
import java.util.Set;

public abstract class Pet {
    private PetSpecies species;
    private String nickname;
    private int age;
    private int tricklevel;
    private Set<String> habits;
    private Family family;

    public Pet(){
        this.species = PetSpecies.UNKNOWN;
        this.nickname = "undefined";
    }
    public Pet(String nickname){
        this.species = PetSpecies.UNKNOWN;
        this.nickname = nickname;
    }
    public Pet(String nickname, int age, int tricklevel, Set<String> habits){
        this.species = PetSpecies.UNKNOWN;
        this.nickname = nickname;
        this.age = age;
        this.tricklevel = tricklevel;
        this.habits = habits;
    }

    void eat(){
        System.out.println("\nI`m eating!");
    }

    abstract void respond();

    public String getPetName(){
        return nickname;
    }

    public String getPetSpecies(){
        return species.getPetSpecies();
    }
    public void setPetSpecies(PetSpecies species){
        this.species = species;
    }
    public int getPetAge(){
        return age;
    }
    public String getPetTrickLevel(){
        return tricklevel > 50 ? "very tricky" : "almost not tricky" ;
    }

    @Override
    public String toString() {
        return "{species=%s, nickname='%s', age=%d, tricklevel=%d, habits=%s}".formatted(species.getPetSpecies(), nickname, age, tricklevel, habits);
//        return "%s{nickname='%s', age=%d, tricklevel=%d, habits=%s}".formatted(species.getPetSpecies(), nickname, age, tricklevel, habits);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age && tricklevel == pet.tricklevel && species == pet.species && Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, tricklevel);
    }
}