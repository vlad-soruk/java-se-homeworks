package homework4_5_6_7_8_9_10_11;

import java.util.Set;

public class RoboCat extends Pet{
    public RoboCat(){
        super();
        super.setPetSpecies(PetSpecies.ROBOCAT);
    }
    public RoboCat(String nickname){
        super(nickname);
        super.setPetSpecies(PetSpecies.ROBOCAT);
    }
    public RoboCat(String nickname, int age, int tricklevel, Set<String> habits){
        super(nickname, age, tricklevel, habits);
        super.setPetSpecies(PetSpecies.ROBOCAT);
    }

    @Override
    void respond() {
        if (super.getPetName() == null) {
            System.out.println("\nHello owner! I missed you");
        }
        else {
            System.out.printf("\nHello owner. I`m %s. I missed you!", super.getPetName());
        }
    }
}
