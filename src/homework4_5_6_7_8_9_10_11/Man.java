package homework4_5_6_7_8_9_10_11;

import java.text.ParseException;
import java.util.Map;

public final class Man extends Human{
    public Man(String name, String surname, String birthDate) throws ParseException {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, String birthDate, Human mother, Human father) throws ParseException {
        super(name, surname, birthDate, mother, father);
    }
    public Man(String name, String surname, String birthDate, int iq, Pet pet) throws ParseException {
        super(name, surname, birthDate, iq, pet);
    }

    public Man(String name, String surname, String birthDate, int iq, Pet pet, Human mother, Human father, Map<String, String> schedule) throws ParseException {
        super(name, surname, birthDate, iq, pet, mother, father, schedule);
    }

    public Man() {
        super();
    }

    @Override
    void greetPet() {
        for ( Pet pet : super.getPets() ) {
            System.out.printf("\nHello %s! How are u doing?", pet.getPetName());
        }
    }

    public void repairCar(){
        System.out.println("\nI`m repairing my car...");
    }
    public void buildingTheHouse(){
        System.out.println("\nI`m building the house...");
    }
}
