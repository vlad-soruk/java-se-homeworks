package homework4_5_6_7_8_9_10_11;

import java.util.Set;

public class Fish extends Pet{
    public Fish(){
        super();
        super.setPetSpecies(PetSpecies.FISHES);
    }
    public Fish(String nickname){
        super(nickname);
        super.setPetSpecies(PetSpecies.FISHES);
    }
    public Fish(String nickname, int age, int tricklevel, Set<String> habits){
        super(nickname, age, tricklevel, habits);
        super.setPetSpecies(PetSpecies.FISHES);
    }

    @Override
    void respond() {
        if(super.getPetName() == null) {
            System.out.println("\nHello owner! I missed you.");
        }
        else {
            System.out.printf("\nHello owner. I`m %s. I missed you!", super.getPetName());
        }
    }
}
