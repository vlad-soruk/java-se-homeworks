package homework4_5_6_7_8_9_10_11;

import homework4_5_6_7_8_9_10_11.FamilyDAO.CollectionFamilyDao;
import homework4_5_6_7_8_9_10_11.FamilyDAO.FamilyController;
import homework4_5_6_7_8_9_10_11.FamilyDAO.FamilyService;

import java.text.ParseException;
import java.util.*;

public class Main {
    public static void main(String[] args) throws ParseException {
//        ----- Describing of human`s age -----
        Human human1 = new Human("Igor", "Lobzin", "22/01/2000");
        System.out.println(human1.describeAge());

//        ----- Initializing of DAO pattern -----
        CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(collectionFamilyDao);
        FamilyController familyController = new FamilyController(familyService);

        ArrayList<Family> allFamilies = familyController.getAllFamilies();

//        System.out.println(allFamilies);

        familyController.displayAllFamilies();

        /* ----- Getting families with more or less members than a particular number ----- */
//        List<Family> familiesBiggerThan = familyController.getFamiliesBiggerThan(4);
//        List<Family> familiesLessThan = familyController.getFamiliesLessThan(6);

        /* -----
                 Getting number of families quantity of members of which
                 equals particular number
           ----- */
//        System.out.println(familyController.countFamiliesWithMemberNumber(5));

        /* ----- Creating a new family ----- */
//        Human humanForNewFamily1 = new Human("Sonya", "Loza", "02/01/2001");
//        Human humanForNewFamily2 = new Human("Sergiy", "Loza", "31/05/2002");
//        familyController.createNewFamily(humanForNewFamily1, humanForNewFamily2);
//        familyController.displayAllFamilies();

        /* ----- Deleting a family by index ----- */
//        familyController.deleteFamilyByIndex(0);
//        familyController.displayAllFamilies();


        /* ----- Deleting a family with family parameter ----- */
//        Human mother1 = new Human("Olesya", "Ternova", "12/11/1984");
//        Human father1 = new Human("Oleksandr", "Ternoviy", "01/10/1981");
//        Human human1OfFamilyToDelete = new Human("Alina", "Ternova", "10/04/2007");
//        Human human2OfFamilyToDelete = new Human("Andriy", "Ternoviy", "27/08/2018");
//        Human human3OfFamilyToDelete = new Human("Sergiy", "Ternoviy", "23/01/2003");
//
//        Family familyToDelete = new Family(mother1, father1);
//        familyToDelete.addChild(human1OfFamilyToDelete);
//        familyToDelete.addChild(human2OfFamilyToDelete);
//        familyToDelete.addChild(human3OfFamilyToDelete);
//
//        System.out.println("\n----- ALL FAMILIES BEFORE DELETING -----");
//        familyController.displayAllFamilies();
//        familyController.deleteFamily(familyToDelete);
//        System.out.println("\n----- ALL FAMILIES AFTER DELETING -----");
//        familyController.displayAllFamilies();


        /* ----- Borning a child ----- */
//        System.out.println(familyController.getAllFamilies().get(0).getPetInfo());
//        Family familyWithNewbornChild = familyController.bornChild(familyController.getAllFamilies().get(0), "Vladyslav", "Anna");
//        System.out.println(familyWithNewbornChild);
//        System.out.println("\n----- ALL FAMILIES AFTER THE FIRST FAMILY BORN A CHILD -----");
//        familyController.displayAllFamilies();

        /* ----- Adopting a child ----- */
//        Human adoptedChild = new Human("Maria", "Romanivna", "20/10/2010");
//        Family familyWithAdoptedChild = familyController.adoptChild(familyController.getAllFamilies().get(0), adoptedChild);
//        System.out.println(familyWithAdoptedChild);
//        familyController.displayAllFamilies();


        /* ----- Deleting all children older than ----- */
//        familyController.displayAllFamilies();
//        familyController.deleteAllChildrenOlderThan(19);
//        familyController.displayAllFamilies();


        /* ----- Quantity of families in database ----- */
//        System.out.println(familyController.count());

        /* ----- Getting family by index ----- */
//        Family familyById = familyController.getFamilyById(1);
//        System.out.println(familyById);

        /* ----- Getting pets of family ----- */
//        System.out.println(familyController.getPets(0));
//        System.out.println(familyController.getPets(1));

        /* ----- Adding pets to family ----- */
//        Pet petToAdd = new DomesticCat("Romeo");
//        familyController.addPet(1, petToAdd);
//        familyController.displayAllFamilies();












//        Set<String> pet1Habits = new HashSet<>(Arrays.asList("eating", "sleeping", "running", "barking"));
//
//        Dog pet1 = new Dog("Archi", 5, 52, pet1Habits);
//        pet1.eat();
//        pet1.foul();
//        pet1.respond();
//
//        Map<String, String> schedule = new HashMap<>();
//        schedule.put(DayOfWeek.MONDAY.getDayOfWeek(), "go to the cryptotrading courses");
//        schedule.put(DayOfWeek.THURSDAY.getDayOfWeek(), "go to the pool");
//        schedule.put(DayOfWeek.FRIDAY.getDayOfWeek(), "go to the yoga");
//        schedule.put(DayOfWeek.SUNDAY.getDayOfWeek(), "go to the yoga");
//
////        {
////            {DayOfWeek.MONDAY.getDayOfWeek(), "go to the cryptotrading courses"},
////            {DayOfWeek.THURSDAY.getDayOfWeek(), "go to the pool"},
////            {DayOfWeek.FRIDAY.getDayOfWeek(), "go to the yoga"},
////            {DayOfWeek.SUNDAY.getDayOfWeek(), "go to the gym"},
////        };
//
//        System.out.println(pet1);
//        Human mother1 = new Human("Katya", "Kozak", 44);
//        Human father1 = new Human("Oleg", "Kozak", 50);
//        Human human1 = new Human("Sasha", "Kozak", 20, 65, pet1, mother1, father1, schedule);
//        System.out.println(human1);
//
//        Set<String> pet2Habits = new HashSet<>(Arrays.asList("scratching", "jumping", "hunting"));
////        String[] pet2Habits = {"scratching", "jumping", "hunting"};
//        Pet pet2 = new RoboCat("Ramos", 10, 10, pet2Habits);
//        System.out.println(pet2);
//
//        Human mother2 = new Human();
//        Human father2 = new Human();
//
//        Human human2 = new Human("Volodya", "Zaraz", 4, 55, pet2, mother1, father1, schedule);
//        System.out.println(human2);
//
//        Family family1 = new Family(mother1, father1);
//        System.out.println(family1);
//
//        family1.addChild(human1);
//        System.out.println(family1);
//
//        family1.addChild(human2);
//
//        System.out.println(family1);
//
//        Family human1Family = human1.getFamily();
//        System.out.print("\nH u m a n  1  f a m i l y:");
//        System.out.println(human1Family);
//
//        int membersOfFamily1Count = family1.countFamily();
//        System.out.printf("\nC o u n t  o f  f a m i l y : %s", membersOfFamily1Count);
//
//        family1.deleteChild(0);
//        System.out.println(family1);
//
//
//        human1.greetPet();
//        human1.describePet();
//        human2.greetPet();
//        human2.describePet();
//
//        Pet pet1Copy = new Dog("Archi", 5, 52, pet1Habits);
////        System.out.println(pet1.equals(pet2));
////        System.out.println(pet1.equals(pet1Copy));
//
//
//        Man man1 = new Man("Volodya", "Zaraz", 4, 55, pet2, mother1, father1, schedule);
////        man1.repairCar();
////        man1.greetPet();
//
//        Woman woman1 = new Woman("Alya", "Vitos", 4, 33, pet2, mother1, father1, schedule);
////        woman1.takeChildrenToKindergarten();
////        woman1.greetPet();
////        woman1.describePet();
//
//        family1.bornChild();
//        System.out.println(family1);
//        family1.bornChild();
//        System.out.println(family1);
    }
}

