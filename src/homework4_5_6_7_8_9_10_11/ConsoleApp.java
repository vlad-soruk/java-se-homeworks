package homework4_5_6_7_8_9_10_11;

import homework4_5_6_7_8_9_10_11.FamilyDAO.CollectionFamilyDao;
import homework4_5_6_7_8_9_10_11.FamilyDAO.FamilyController;
import homework4_5_6_7_8_9_10_11.FamilyDAO.FamilyService;

import java.io.*;
import java.text.ParseException;
import java.util.*;

public class ConsoleApp {
    private static int familyDataBaseSize;
    private static String filePath = "./src/homework4_5_6_7_8_9_10_11/newFileWithDatabase.txt";

    public static void main(String[] args) throws ParseException, IOException {
        FamilyController familyController = initializeFamilyController();

//        Створюємо Сканер один раз для всього додатку
         Scanner scanner = new Scanner(System.in);
        label:
        while(true) {
            printAvailableCommands();
            System.out.println("Введіть номер команди (або exit для виходу):");
            String userCommand = getAndValidateUserInput(scanner);

            switch (userCommand) {
                case "1" -> saveDataLocally(familyController);
                case "2" -> loadSavedData(familyController);
                case "3" -> fillDatabaseWithTestFamilies(familyController);
                case "4" -> displayAllFamiliesInDatabase(familyController);
                case "5" -> displayFamiliesBiggerThan(familyController, scanner);
                case "6" -> displayFamiliesLessThan(familyController, scanner);
                case "7" -> countFamiliesWithMemberNumber(familyController, scanner);
                case "8" -> createNewFamilyInDatabase(familyController, scanner);
                case "9" -> deleteFamilyByIndex(familyController, scanner);
                case "10" -> changeFamilyInfo(familyController, scanner);
                case "11" -> deleteChildrenOlderThan(familyController, scanner);
                case "exit" -> {
                    System.out.println("EXITING...");
                    break label;
                }
                default -> System.out.println("Непідходяще значення вводу");
            }
        }

        scanner.close();
    }

    private static void loadSavedData(FamilyController familyController) {
        try(FileInputStream fileInputStream = new FileInputStream(filePath);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);){
            List<Family> families = (List<Family>) objectInputStream.readObject();
            familyController.loadData(families);
            System.out.println("Успішно завантажили дані в базу даних");
        }
        catch (IOException | ClassNotFoundException e){
            e.getStackTrace();
        }
    }

    private static void saveDataLocally(FamilyController familyController) {
        try(FileOutputStream fileOutputStream = new FileOutputStream(filePath);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)){
//            Інформація записується в бінарному форматі
            objectOutputStream.writeObject(familyController.getAllFamilies());
            System.out.println("Успішно завантажили дані з бази даних локально в файл");
        }
        catch(IOException e) {
            e.getStackTrace();
        }
    }

    private static void deleteChildrenOlderThan(FamilyController familyController, Scanner scanner) {
        System.out.println("Введіть вік дітей, яких хочете видалити із сім'ї...");
        int userInputChildAge;
        while (true) {
            try {
                userInputChildAge = scanner.nextInt();
                if(userInputChildAge < 0 || userInputChildAge>100) {
                    throw new NumberFormatException();
                }
                break;
            }
            catch(InputMismatchException e) {
                scanner.nextLine();
                System.out.println("Введіть число, будь ласка...");
            }
            catch(NumberFormatException e) {
                scanner.nextLine();
                System.out.println("Число повинно бути від 0 до 100 включно. Введіть ще раз...");
            }
        }

        familyController.deleteAllChildrenOlderThan(userInputChildAge);
        familyController.displayAllFamilies();
    }

    private static void changeFamilyInfo(FamilyController familyController, Scanner scanner) throws ParseException {
        System.out.println("Введіть 1, якщо хочете додати дитину сім'ї");
        System.out.println("Введіть 2, якщо хочете додати усиновлену дитину сім'ї");
        System.out.println("Або введіть 3, щоб повернутися назад");
        int userInputNumber;
        int userInputFamilyId;
        String userInputNameOfBoy;
        String userInputNameOfGirl;
        while (true) {
            try {
                userInputNumber = scanner.nextInt();
                if(userInputNumber != 1 && userInputNumber != 2 && userInputNumber != 3) {
                    throw new NumberFormatException();
                }
                break;
            }
            catch(InputMismatchException e) {
                scanner.nextLine();
                System.out.println("Введіть число, будь ласка...");
            }
            catch(NumberFormatException e) {
                scanner.nextLine();
                System.out.println("Число повинно бути або 1, або 2, або 3. Введіть ще раз...");
            }
        }
        if (userInputNumber == 1) {
            System.out.println("Введіть індекс сім'ї, яку хочете редагувати...");
            userInputFamilyId = getAndValidateUserInputOfFamilyId(scanner);
            System.out.println("Введіть потенційне ім'я, якщо буде хлопчик...");
            userInputNameOfBoy = getAndValidateName(scanner);
            System.out.println("Введіть потенційне ім'я, якщо буде дівчинка...");
            userInputNameOfGirl = getAndValidateName(scanner);
            familyController.bornChild(familyController.getFamilyById(userInputFamilyId - 1), userInputNameOfBoy, userInputNameOfGirl);
            System.out.printf("Дитина для сім'ї з індексом %d успішно додана", userInputFamilyId);
            familyController.displayAllFamilies();
        }
        else if (userInputNumber == 2) {
            System.out.println("Введіть індекс сім'ї, яку хочете редагувати...");
            userInputFamilyId = getAndValidateUserInputOfFamilyId(scanner);
            System.out.println("Введіть ім'я усиновленої дитини...");
            String userInputChildName = getAndValidateName(scanner);
            System.out.println("Введіть прізвище усиновленої дитини...");
            String userInputChildSurname = getAndValidateName(scanner);
            System.out.println("Введіть рік усиновленої дитини (у форматі dd/mm/yy, наприклад, 05/08/2003)...");
            String userInputDateOfChild = getAndValidateUserDateInput(scanner);
            System.out.println("Введіть айк'ю усиновленої дитини (в межах від 1 до 200 включно)...");
            int userInputIqOfChild = getAndValidateUserIqInput(scanner);

            Human adoptedHuman = new Human(userInputChildName, userInputChildSurname, userInputDateOfChild, userInputIqOfChild);
            familyController.adoptChild(familyController.getFamilyById(userInputFamilyId - 1), adoptedHuman);

            familyController.displayAllFamilies();
        }
    }

    private static void deleteFamilyByIndex(FamilyController familyController, Scanner scanner) {
        System.out.println("Введіть індекс сім'ї, яку хочете видалити...");
        int userInputFamilyId = getAndValidateUserInputOfFamilyId(scanner);
        familyController.deleteFamilyByIndex(userInputFamilyId - 1);
        System.out.printf("Родина під індексом %d успішно видалилася", userInputFamilyId);
        familyController.displayAllFamilies();
        //                Оновлюємо в полі familyDataBaseSize кількість сімей в базі даних
        familyDataBaseSize = familyController.count();
    }

    private static void createNewFamilyInDatabase(FamilyController familyController, Scanner scanner) throws ParseException {
        System.out.println("Введіть ім'я матері...");
        String userInputMotherName = getAndValidateName(scanner);
        System.out.println("Введіть прізвище матері...");
        String userInputMotherSurname = getAndValidateName(scanner);
        System.out.println("Введіть рік народження матері (у форматі dd/mm/yy, наприклад, 05/08/2003)...");
        String userInputDateOfMother = getAndValidateUserDateInput(scanner);
        System.out.println("Введіть айк'ю матері (в межах від 1 до 200 включно)...");
        int userInputIqOfMother = getAndValidateUserIqInput(scanner);

        System.out.println("Введіть ім'я батька...");
        String userInputFatherName = getAndValidateName(scanner);
        System.out.println("Введіть прізвище батька...");
        String userInputFatherSurname = getAndValidateName(scanner);
        System.out.println("Введіть рік народження батька (у форматі dd/mm/yy, наприклад, 05/08/2003)...");
        String userInputDateOfFather = getAndValidateUserDateInput(scanner);
        System.out.println("Введіть айк'ю батька (в межах від 1 до 200 включно)...");
        int userInputIqOfFather = getAndValidateUserIqInput(scanner);

        Human mother = new Human(userInputMotherName, userInputMotherSurname, userInputDateOfMother, userInputIqOfMother);
        Human father= new Human(userInputFatherName, userInputFatherSurname, userInputDateOfFather, userInputIqOfFather);

        Family createdFamily = new Family(mother, father);
        familyController.saveFamily(createdFamily);

//                Оновлюємо в полі familyDataBaseSize кількість сімей в базі даних
        familyDataBaseSize = familyController.count();
        System.out.println("Нова родина успішно створена");
        familyController.displayAllFamilies();
    }

    private static void countFamiliesWithMemberNumber(FamilyController familyController, Scanner scanner) {
        System.out.println("Введіть кількість членів родини...");
        String userNumber = getAndValidateUserInputOfMembersOfFamily(scanner);
        System.out.printf("Кількість сімей, кількість членів якої дорівнює %d: %d",
                Integer.parseInt(userNumber),
                familyController.countFamiliesWithMemberNumber(Integer.parseInt(userNumber)));
    }

    private static void displayFamiliesLessThan(FamilyController familyController, Scanner scanner) {
        System.out.println("Введіть кількість членів родини...");
        String userNumber = getAndValidateUserInputOfMembersOfFamily(scanner);
        familyController.getFamiliesLessThan(Integer.parseInt(userNumber));
    }

    private static void displayFamiliesBiggerThan(FamilyController familyController, Scanner sc) {
        System.out.println("Введіть кількість членів родини...");
        String userNumber = getAndValidateUserInputOfMembersOfFamily(sc);
        familyController.getFamiliesBiggerThan(Integer.parseInt(userNumber));
    }

    private static void displayAllFamiliesInDatabase(FamilyController familyController) {
        if (familyController.count() == 0) {
            System.out.println("На жаль, в базі даних не має сімей");
        }
        else {
            System.out.println("Відображаю весь список сімей...");
            familyController.displayAllFamilies();
        }
    }

    private static void fillDatabaseWithTestFamilies(FamilyController familyController) throws ParseException {

        Map<String, String> scheduleOfMother = new HashMap<>();
        scheduleOfMother.put(DayOfWeek.FRIDAY.getDayOfWeek(), "fitness");
        scheduleOfMother.put(DayOfWeek.MONDAY.getDayOfWeek(), "fitness");

        Map<String, String> scheduleOfFather = new HashMap<>();
        scheduleOfFather.put(DayOfWeek.FRIDAY.getDayOfWeek(), "library");
        scheduleOfFather.put(DayOfWeek.MONDAY.getDayOfWeek(), "library");

        Set<String> petOfMotherHabits = new HashSet<>();
        petOfMotherHabits.add("sleep");

        Pet petOfMother = new Dog("Jack", 3, 35, petOfMotherHabits);

        Set<String> petOfFatherHabits = new HashSet<>();
        petOfFatherHabits.add("eat");
        petOfFatherHabits.add("play");

        Pet petOfFather = new DomesticCat("Oscar", 5, 81, petOfFatherHabits);


        Human mother1 = new Human("Kate", "Bibo", "03/03/1991", 95, scheduleOfMother, petOfMother);
        Human father1 = new Human("Karl", "Bibo", "10/12/1990", 90, scheduleOfFather, petOfFather);

        Human child1 = new Human("Donna", "Bibo", "23/10/2018", 92);
        Human child2 = new Human("Sun", "Bibo", "23/10/2018", 92);
        Map<String, String> scheduleOfchild3 = new HashMap<>();
        scheduleOfchild3.put(DayOfWeek.FRIDAY.getDayOfWeek(), "music");
        Human child3 = new Human("Kurt", "Kobein", "05/05/2003", 85, scheduleOfchild3, null);

        Family family1 = new Family(mother1, father1);
        family1.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child3);

        Human mother2 = new Human("Helena", "Darco", "23/10/2000", 87);
        Human father2 = new Human("Donni", "Darco", "01/12/1999", 89);
        Family family2 = new Family(mother2, father2);
        Human firstChildOfFamily2 = new Human("Romeo", "Darco", "12/12/2022");
        family2.addChild(firstChildOfFamily2);

        Human mother3 = new Human("Sarah", "Kristy", "11/07/1975", 97);
        Human father3 = new Human("Alfred", "Kristy", "01/03/1980", 99);
        Family family3 = new Family(mother3, father3);
        Human firstChildOfFamily3 = new Human("Greg", "Kristy", "19/01/2000");
        Human secondChildOfFamily3 = new Human("Jessica", "Kristy", "29/11/2002");
        Human thirdChildOfFamily3 = new Human("Marta", "Kristy", "20/09/2007");
        Human fourthChildOfFamily3 = new Human("Samara", "Kristy", "22/03/2009");
        family3.addChild(firstChildOfFamily3);
        family3.addChild(secondChildOfFamily3);
        family3.addChild(thirdChildOfFamily3);
        family3.addChild(fourthChildOfFamily3);

        familyController.saveFamily(family1);
        familyController.saveFamily(family2);
        familyController.saveFamily(family3);
        familyController.displayAllFamilies();

//          Оновлюємо в полі familyDataBaseSize кількість сімей в базі даних
        familyDataBaseSize = familyController.count();
    }

    private static int getAndValidateUserInputOfFamilyId(Scanner sc) {
        int userInput;

        while (true) {
            try {
                userInput = sc.nextInt();
                if (userInput <= 0 || userInput > familyDataBaseSize) {
                    throw new IndexOutOfBoundsException();
                }
//                Чистимо буфер ввода в сканері
                sc.nextLine();
                break;
            }
            catch (InputMismatchException e) {
                sc.nextLine();
                System.out.println("Введіть число, будь ласка...");
            }
            catch (IndexOutOfBoundsException e) {
                sc.nextLine();
                System.out.println("Введіть підходящий індекс, будь ласка...");
            }
        }
        return userInput;
    }

    private static int getAndValidateUserIqInput(Scanner sc) {
        String userInput;
        while (true) {
            userInput = sc.nextLine().toLowerCase().replaceAll(" ","");

            if(userInput.equals("exit")) {
                break;
            }
            try {
                int userInputInt = Integer.parseInt(userInput);
                if (userInputInt >= 1 && userInputInt <= 200) {
                    break;
                }
                else {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                sc.nextLine();
                System.out.println("Будь ласка, введіть числове значення в межах від 1 до 200 включно...");
            }
        }
        sc.nextLine();
        return Integer.parseInt(userInput);
    }

    private static String getAndValidateName(Scanner sc) {
        String userInput;
        while (true) {
            userInput = sc.nextLine().replaceAll(" ", "");
            if(userInput.equalsIgnoreCase("exit")) {
                break;
            }
            else if(!userInput.matches(".*\\d.*")) {
                break;
            }
            else {
                System.out.println("Непідходяще значення вводу. Будь ласка, спробуйте ще раз...");
            }
        }

        return userInput;
    }

    private static String getAndValidateUserDateInput(Scanner sc) {
        String userInput;

        while (true) {
            userInput = sc.nextLine().replaceAll(" ","");

            if (userInput.equals("exit")) {
                break;
            }

//            Формат дати повинен бути dd/mm/yyyy
            else if (userInput.matches("\\d{2}/\\d{2}/\\d{4}")) {
                break;
            } else {
                System.out.println("Будь ласка введіть дату народження в форматі 'dd/mm/yyyy'...");
            }
        }

        return userInput;
    }


    private static String getAndValidateUserInputOfMembersOfFamily(Scanner sc) {
        String userInput;
//        За замовчуванням пишемо, що userInputIsValid = false
        boolean userInputIsValid = false;

        while(true){
            userInput = sc.nextLine().toLowerCase().replaceAll(" ","");

            if(userInput.equals("exit")) {
                userInputIsValid = true;
                break;
            }
            try {
                int userInputInt = Integer.parseInt(userInput);
                if (userInputInt >= 1) {
                    userInputIsValid = true;
                    break;
                }
                else {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                sc.nextLine();
                System.out.println("Будь ласка, введіть числове значення більше або рівне 1...");
            }
        }

        return userInput;
    }

    private static void printAvailableCommands() {
        StringBuilder sb = new StringBuilder("\n- 1. Записати дані з бази даних локально в файл\n");
        sb.append("- 2. Завантажити дані з файлу в базу даних\n");
        sb.append("- 3. Заповнити тестовими даними\n");
        sb.append("- 4. Відобразити весь список сімей\n");
        sb.append("- 5. Відобразити список сімей, де кількість людей більша за задану\n");
        sb.append("- 6. Відобразити список сімей, де кількість людей менша за задану\n");
        sb.append("- 7. Підрахувати кількість сімей, де кількість членів дорівнює конкрентному числу\n");
        sb.append("- 8. Створити нову родину\n");
        sb.append("- 9. Видалити сім'ю за індексом сім'ї у загальному списку\n");
        sb.append("- 10. Редагувати сім'ю за індексом сім'ї у загальному списку\n");
        sb.append("\t- 1. Народити дитину\n");
        sb.append("\t- 2. Усиновити дитину\n");
        sb.append("\t- 3. Повернутися до головного меню\n");
        sb.append("- 11. Видалити всіх дітей старше конкретного віку\n");
        System.out.println(sb.toString());
    }

    private static FamilyController initializeFamilyController() throws ParseException {
        CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(collectionFamilyDao);
        FamilyController familyController = new FamilyController(familyService);
        familyDataBaseSize = familyController.count();
        return familyController;
    }

    private static String getAndValidateUserInput(Scanner sc){
        String userInput;
//        За замовчуванням пишемо, що userInputIsValid = false
        boolean userInputIsValid = false;

        while(true){
            userInput = sc.nextLine().toLowerCase().replaceAll(" ","");

            if(userInput.equals("exit")) {
                userInputIsValid = true;
                break;
            }
            try {
                int userInputInt = Integer.parseInt(userInput);
                if (userInputInt >= 1) {
                    userInputIsValid = true;
                    break;
                }
                else {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                System.out.println("Будь ласка, введіть числове значення (в діапазоні не менше від 1)...");
            }
        }

        return userInput;
    }
}
