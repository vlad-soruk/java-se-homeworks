package homework4_5_6_7_8_9_10_11.FamilyDAO;

import homework4_5_6_7_8_9_10_11.Family;
import java.util.ArrayList;
import java.util.List;

interface FamilyDao {
    ArrayList<Family> getAllFamilies();
    Family getFamilyByIndex(int index) ;
    boolean deleteFamily(int index);
    boolean deleteFamily(Family family);
    /**
    * Updates family if it already exists or adds it to the end of
    * a list if it doesn`t exist
    * */
    void saveFamily(Family family);
    void loadData(List<Family> families);
}
