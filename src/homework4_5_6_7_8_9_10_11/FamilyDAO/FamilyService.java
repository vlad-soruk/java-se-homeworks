package homework4_5_6_7_8_9_10_11.FamilyDAO;

import homework4_5_6_7_8_9_10_11.Family;
import homework4_5_6_7_8_9_10_11.Human;
import homework4_5_6_7_8_9_10_11.Pet;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FamilyService {
    private FamilyDao familyDao;
    public FamilyService(FamilyDao familyDao){
        this.familyDao = familyDao;
    }
    public ArrayList<Family> getAllFamilies(){
        return familyDao.getAllFamilies();
    }
    public void displayAllFamilies(){
//        Рефакторинг згідно з Java 8
        familyDao.getAllFamilies().forEach(family -> System.out.printf(
                "\n%d: %s\n", familyDao.getAllFamilies().indexOf(family) + 1, family.prettyFormat()));
    }
    public List<Family> getFamiliesBiggerThan(int familyMembersCount){
//        Рефакторинг згідно з Java 8
        ArrayList<Family> allFamilies = familyDao.getAllFamilies();

//        Перетворюємо список всіх сімей в потік та фільтруємо по кількості членів сім'ї
        List<Family> result = allFamilies.stream()
                .filter(family -> family.countFamily() > familyMembersCount)
                .collect(Collectors.toList());

        if(result.isEmpty()) {
            System.out.printf(
                    "\nThere are no families quantity of members of which is more than %d\n",
                    familyMembersCount);
        }
        else {
            result.forEach(f ->
                    System.out.printf("\nFamily %d, quantity of members of which is bigger than %d: %s\n",
                                        result.indexOf(f) + 1, familyMembersCount, f));
        }

        return result;
    }
    public List<Family> getFamiliesLessThan(int familyMembersCount){
//        Рефакторинг згідно з Java 8
        ArrayList<Family> allFamilies = familyDao.getAllFamilies();

//        Перетворюємо список всіх сімей в потік та фільтруємо по кількості членів сім'ї
        List<Family> result = allFamilies.stream()
                .filter(family -> family.countFamily() < familyMembersCount)
                .collect(Collectors.toList());

        if(result.isEmpty()) {
            System.out.printf(
                    "There are no families quantity of members of which is less than %d\n",
                    familyMembersCount);
        }
        else {
            result.forEach(f ->
                    System.out.printf("Family %d, quantity of members of which is less than %d: %s\n",
                            result.indexOf(f) + 1, familyMembersCount, f));
        }

        return result;
    }
    public int countFamiliesWithMemberNumber(int memberNumber){
//        Рефакторинг згідно з Java 8
        ArrayList<Family> allFamilies = familyDao.getAllFamilies();

        List<Family> result = allFamilies.stream()
                .filter(f -> f.countFamily() == memberNumber)
                .collect(Collectors.toList());

        return result.size();
    }
    public void createNewFamily(Human mother, Human father){
        Family newFamily = new Family(mother, father);
        familyDao.saveFamily(newFamily);
    }
    public void deleteFamilyByIndex(int index){
        familyDao.deleteFamily(index);
    }

    public void deleteFamily(Family family) {
        familyDao.deleteFamily(family);
    }
    public Family bornChild(Family family, String boyName, String girlName) throws ParseException {
        ArrayList<Family> allFamilies = familyDao.getAllFamilies();
        if ( !allFamilies.contains(family) ) {
            System.out.println("There are no such family in database yet, add it to database first!");
        }
        else {
            family.bornChild(boyName, girlName);
//            Оновлюємо сім'ю в "базі даних"
            familyDao.saveFamily(family);
        }
        return family;
    }
    public Family adoptChild(Family family, Human human){
        ArrayList<Family> allFamilies = familyDao.getAllFamilies();
        if ( !allFamilies.contains(family) ) {
            System.out.println("There are no such family in database yet, add it to database first!");
        }
        else {
            family.addChild(human);
//            Оновлюємо сім'ю в "базі даних"
            familyDao.saveFamily(family);
        }
        return family;
    }
    public void deleteAllChildrenOlderThan(int age){
//        Рефакторинг згідно з Java 8
        ArrayList<Family> allFamilies = familyDao.getAllFamilies();
        allFamilies.forEach(f -> f.getChildrenInfo().removeIf(child -> child.getYear() > age));
    }
    public int count(){
        return familyDao.getAllFamilies().size();
    }
    public Family getFamilyById(int index){
        return familyDao.getFamilyByIndex(index);
    }
    public ArrayList<Pet> getPets(int index){
        ArrayList<Family> allFamilies = familyDao.getAllFamilies();
        Family family = allFamilies.get(index);
        return family.getPetInfo();
    }
    public void addPet(int index, Pet pet){
        ArrayList<Family> allFamilies = familyDao.getAllFamilies();
        Family family = allFamilies.get(index);
        family.addPet(pet);
        familyDao.saveFamily(family);
    }

    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }

    public void loadData(List<Family> families) {
        familyDao.loadData(families);
    }
}
