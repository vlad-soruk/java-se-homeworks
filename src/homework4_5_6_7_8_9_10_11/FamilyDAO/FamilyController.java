package homework4_5_6_7_8_9_10_11.FamilyDAO;

import homework4_5_6_7_8_9_10_11.Family;
import homework4_5_6_7_8_9_10_11.FamilyOverflowException;
import homework4_5_6_7_8_9_10_11.Human;
import homework4_5_6_7_8_9_10_11.Pet;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class FamilyController {
    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public ArrayList<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies(){
        familyService.displayAllFamilies();
    }
    public List<Family> getFamiliesBiggerThan(int familyMembersCount){
        return familyService.getFamiliesBiggerThan(familyMembersCount);
    }
    public List<Family> getFamiliesLessThan(int familyMembersCount){
        return familyService.getFamiliesLessThan(familyMembersCount);
    }
    public int countFamiliesWithMemberNumber(int memberNumber){
        return familyService.countFamiliesWithMemberNumber(memberNumber);
    }
    public void createNewFamily(Human mother, Human father){
        familyService.createNewFamily(mother, father);
    }
    public void deleteFamilyByIndex(int index){
        familyService.deleteFamilyByIndex(index);
    }
    public void deleteFamily(Family family){
        familyService.deleteFamily(family);
    }
    public Family bornChild(Family family, String boyName, String girlName) throws FamilyOverflowException, ParseException {
        try {
            if ((family.countFamily() - 2) < 5) {
                familyService.bornChild(family, boyName, girlName);
            }
            else {
                throw new FamilyOverflowException();
            }
        }
        catch (FamilyOverflowException e) {
            e.printStackTrace();
        }

        return family;
    }
    public Family adoptChild(Family family, Human human) throws FamilyOverflowException{
        try {
            if ((family.countFamily() - 2) < 5) {
                familyService.adoptChild(family, human);
                System.out.printf("Усиновлена дитина для сім'ї з індексом %d успішно додана", familyService.getAllFamilies().indexOf(family) + 1);
            }
            else {
                throw new FamilyOverflowException();
            }
        }
        catch (FamilyOverflowException e) {
            e.printStackTrace();
        }

        return family;
    }
    public void deleteAllChildrenOlderThan(int age){
        familyService.deleteAllChildrenOlderThan(age);
    }
    public int count(){
        return familyService.getAllFamilies().size();
    }
    public Family getFamilyById(int index){
        return familyService.getFamilyById(index);
    }
    public ArrayList<Pet> getPets(int index){
        return familyService.getPets(index);
    }
    public void addPet(int index, Pet pet){
        familyService.addPet(index, pet);
    }
    public void saveFamily(Family family){
        familyService.saveFamily(family);
    }
    public void loadData(List<Family> families){
        familyService.loadData(families);
    }
}