package homework4_5_6_7_8_9_10_11.FamilyDAO;

import homework4_5_6_7_8_9_10_11.Dog;
import homework4_5_6_7_8_9_10_11.Family;
import homework4_5_6_7_8_9_10_11.Human;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    private ArrayList<Family> families = new ArrayList<>();
    public CollectionFamilyDao() throws ParseException {
//        Our "database" of families
        Dog pet1 = new Dog("Archi");
        Human mother1 = new Human("Katya", "Kozak", "22/02/1980", 65, pet1);
        Human father1 = new Human("Oleg", "Kozak", "11/07/1973");
        Human human1 = new Human("Sasha", "Kozak", "02/04/2002");
        Human human2 = new Human("Volodya", "Zaraz", "28/03/2019");
        Family family1 = new Family(mother1, father1);
        family1.addChild(human1);
        family1.addChild(human2);

        Human mother2 = new Human("Olesya", "Ternova", "12/11/1984");
        Human father2 = new Human("Oleksandr", "Ternoviy", "01/10/1981");
        Human human3 = new Human("Alina", "Ternova", "10/04/2007");
        Human human4 = new Human("Andriy", "Ternoviy", "27/08/2018");
        Human human5 = new Human("Sergiy", "Ternoviy", "23/01/2003");

        Family family2 = new Family(mother2, father2);
        family2.addChild(human3);
        family2.addChild(human4);
        family2.addChild(human5);

        families.add(family1);
        families.add(family2);
    }
    @Override
    public ArrayList<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index >= 0 && index < families.size()) {
            return families.get(index);
        } else {
            return null;
        }
    }

    @Override
    public boolean deleteFamily(int index) {
        if(index >= 0 && index < families.size()) {
            families.remove(index);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean deleteFamily(Family family) {
        if( families.contains(family) ) {
            families.remove(family);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void saveFamily(Family family) {
        if( families.contains(family) ) {
            int index = families.indexOf(family);
            Family familyToUpdate = families.get(index);
            familyToUpdate.setMother(family.getMotherInfo());
            familyToUpdate.setFather(family.getFatherInfo());
            familyToUpdate.setChildren(family.getChildrenInfo());
            familyToUpdate.setPet(family.getPetInfo());
        }
        else {
            families.add(family);
        }
    }

    @Override
    public void loadData(List<Family> families) {
        families.forEach(family -> {
            if (!families.contains(family)) {
                families.add(family);
            }
        });
    }
}
