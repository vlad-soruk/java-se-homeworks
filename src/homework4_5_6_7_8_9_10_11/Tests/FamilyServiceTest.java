package homework4_5_6_7_8_9_10_11.Tests;

import homework4_5_6_7_8_9_10_11.*;
import homework4_5_6_7_8_9_10_11.FamilyDAO.CollectionFamilyDao;
import homework4_5_6_7_8_9_10_11.FamilyDAO.FamilyService;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {
    private final CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao();
    private final FamilyService familyService = new FamilyService(collectionFamilyDao);

    FamilyServiceTest() throws ParseException {
    }

    @Test
    void getAllFamilies() {
        ArrayList<Family> allFamilies = familyService.getAllFamilies();
        assertNotNull(allFamilies);
    }

    @Test
    void getFamiliesBiggerThan() {
        List<Family> familiesBiggerThan1 = familyService.getFamiliesBiggerThan(100);
        assertEquals(familiesBiggerThan1.size(), 0);
        List<Family> familiesBiggerThan2 = familyService.getFamiliesBiggerThan(2);
        assertEquals(familiesBiggerThan2.size(), 2);
    }

    @Test
    void getFamiliesLessThan() {
        List<Family> familiesLessThan1 = familyService.getFamiliesLessThan(100);
        assertEquals(familiesLessThan1.size(), 2);
        List<Family> familiesLessThan2 = familyService.getFamiliesLessThan(2);
        assertEquals(familiesLessThan2.size(), 0);
    }

    @Test
    void countFamiliesWithMemberNumber() {
        int familyMembersCount1 = familyService.countFamiliesWithMemberNumber(4);
        assertEquals(familyMembersCount1, 1);
        int familyMembersCount2 = familyService.countFamiliesWithMemberNumber(100);
        assertEquals(familyMembersCount2, 0);
    }

    @Test
    void createNewFamily() throws ParseException {
        Human humanForNewFamily1 = new Human("Sonya", "Loza", "01/01/2001");
        Human humanForNewFamily2 = new Human("Sergiy", "Loza", "01/01/2003");
        familyService.createNewFamily(humanForNewFamily1, humanForNewFamily2);
        assertEquals(familyService.getAllFamilies().size(), 3);
    }

    @Test
    void deleteFamilyByIndex() {
        familyService.deleteFamilyByIndex(0);
        assertEquals(familyService.getAllFamilies().size(), 1);
        familyService.deleteFamilyByIndex(0);
        assertEquals(familyService.getAllFamilies().size(), 0);
    }

    @Test
    void bornChild() throws ParseException {
        int initialChildrenCount = familyService.getFamilyById(0).getChildrenInfo().size();
        familyService.bornChild(familyService.getFamilyById(0), "Vasyl", "Alla");
        assertEquals(familyService.getFamilyById(0).getChildrenInfo().size(), initialChildrenCount + 1);
    }

    @Test
    void adoptChild() throws ParseException {
        int initialChildrenCount = familyService.getFamilyById(0).getChildrenInfo().size();
        Human adoptedChild1 = new Human("Maria", "Romanivna", "12/02/2010");
        Human adoptedChild2 = new Human("Ivan", "Krasen", "29/08/2016");
        familyService.adoptChild(familyService.getFamilyById(0), adoptedChild1);
        assertEquals(familyService.getFamilyById(0).getChildrenInfo().size(), initialChildrenCount + 1);
        familyService.adoptChild(familyService.getFamilyById(0), adoptedChild2);
        assertEquals(familyService.getFamilyById(0).getChildrenInfo().size(), initialChildrenCount + 2);
    }

    @Test
    void deleteAllChildrenOlderThan() {
        int initialChildrenCountOfFamily1 = familyService.getFamilyById(0).getChildrenInfo().size();
        int initialChildrenCountOfFamily2 = familyService.getFamilyById(1).getChildrenInfo().size();

        familyService.deleteAllChildrenOlderThan(18);
        assertEquals(familyService.getFamilyById(0).getChildrenInfo().size(), initialChildrenCountOfFamily1 - 1);
        assertEquals(familyService.getFamilyById(1).getChildrenInfo().size(), initialChildrenCountOfFamily2 - 1);
    }

    @Test
    void count() {
        int count = familyService.count();
        assertEquals(count, 2);
    }

    @Test
    void getFamilyById() throws ParseException {
        Dog pet1 = new Dog("Archi");
        Human mother1 = new Human("Katya", "Kozak", "22/02/1980", 65, pet1);
        Human father1 = new Human("Oleg", "Kozak", "11/07/1973");
        Human human1 = new Human("Sasha", "Kozak", "02/04/2002");
        Human human2 = new Human("Volodya", "Zaraz", "28/03/2019");
        Family family1 = new Family(mother1, father1);
        family1.addChild(human1);
        family1.addChild(human2);

        Family familyById = familyService.getFamilyById(0);
        assertEquals(familyById, family1);
    }

    @Test
    void getPets() {
        ArrayList<Pet> petsOfF1 = new ArrayList<>();
        Dog pet1 = new Dog("Archi");
        petsOfF1.add(pet1);
        ArrayList<Pet> petsOfFamily1 = familyService.getPets(0);
        assertEquals(petsOfFamily1, petsOfF1);

        ArrayList<Pet> petsOfFamily2 = familyService.getPets(1);
        assertEquals(petsOfFamily2, new ArrayList<Pet>());

    }

    @Test
    void addPet() {
        ArrayList<Pet> petsOfF1 = new ArrayList<>();
        Pet pet1 = new Fish("Golden");
        Pet pet2 = new RoboCat("Kraken");
        petsOfF1.add(pet1);
        petsOfF1.add(pet2);
        familyService.addPet(1, pet1);
        familyService.addPet(1, pet2);

        ArrayList<Pet> petsOfFamily1 = familyService.getPets(1);
        assertEquals(petsOfFamily1, petsOfF1);
    }
}