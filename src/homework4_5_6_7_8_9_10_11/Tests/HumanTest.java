package homework4_5_6_7_8_9_10_11.Tests;

import homework4_5_6_7_8_9_10_11.Human;
import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {
    private static final Human human1;

    static {
        try {
            human1 = new Human("Roman", "Bull", "12/02/1989");
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void testToString() {
        assertEquals("\nHuman{name='Roman', surname='Bull', date of birth=\"12/02/1989\", iq=Undefined, pet=Undefined pet, mother=Undefined, father=Undefined, schedule=Undefined",
                human1.toString());
//        assertEquals("\nhuman{name='Roman', surname='Bull', year=34, iq=Undefined, pet=Undefined pet, mother=Undefined, father=Undefined, schedule=\"Undefined\"",
//                human.toString());
    }

    @Test
    void testEquals() throws ParseException {
        Human human1Copy = new Human("Roman", "Bull", "12/02/1989");
        Human human2 = new Human("Igor", "Lozbin", "31/08/1967");
        assertEquals(human1Copy, human1);
//        assertEquals(human2, human1);
    }
}