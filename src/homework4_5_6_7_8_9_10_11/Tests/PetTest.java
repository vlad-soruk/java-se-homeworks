package homework4_5_6_7_8_9_10_11.Tests;

import homework4_5_6_7_8_9_10_11.Dog;
import homework4_5_6_7_8_9_10_11.Pet;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {
    private static final Set<String> pet1Habits = new HashSet<>(Arrays.asList("eating", "sleeping", "running", "barking"));

    private static final Pet pet1 = new Dog("Archi", 5, 52, pet1Habits);

    @Test
    void getPetName() {
        String petName = pet1.getPetName();
        assertEquals("Archi", petName);
//        assertEquals("Arch", petName);
    }

    @Test
    void getPetSpecies() {
        String petSpecies = pet1.getPetSpecies();
        assertEquals("Dog", petSpecies);
//        assertEquals("dog", petSpecies);
    }

    @Test
    void getPetAge() {
        int petAge = pet1.getPetAge();
        assertEquals(5, petAge);
//        assertEquals(10, petAge);
    }

    @Test
    void testToString() {
        assertEquals("Dog{nickname='Archi', age=5, tricklevel=52, habits=[running, barking, eating, sleeping]}", pet1.toString());
//        assertEquals("\nDOG{nickname='Archi', age=15, tricklevel=52, habits=[eating, sleeping, running, barking]}", pet1.toString());
    }

    @Test
    void testEquals(){
        Pet pet1Copy = new Dog("Archi", 5, 52, pet1Habits);
        assertEquals(pet1Copy, pet1);
    }
}