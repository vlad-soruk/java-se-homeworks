package homework4_5_6_7_8_9_10_11.Tests;

import homework4_5_6_7_8_9_10_11.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
class FamilyTest {
    private final Human mother1 = new Human("Katya", "Kozak", "06/01/1979");
    private final Human mother2 = new Human("Maria", "Shilovets", "23/05/1979");

    private final Human father1 = new Human("Oleg", "Kozak", "12/12/1973");
    private final Human father2 = new Human("Andrew", "Gubar", "02/09/1973");
    private final Family family = new Family(mother1, father1);

    FamilyTest() throws ParseException {
    }

    @org.junit.jupiter.api.Test
    void testGetMotherInfo() {
        Human motherInfo = family.getMotherInfo();
        assertEquals(motherInfo, mother1);
//        assertEquals(motherInfo, mother2);
    }

    @org.junit.jupiter.api.Test
    void getFatherInfo() {
        Human fatherInfo = family.getFatherInfo();
        assertEquals(fatherInfo, father1);
//        assertEquals(fatherInfo, father2);
    }

    @org.junit.jupiter.api.Test
    void getChildrenInfo() throws ParseException {
        Human child1 = new Human("Bogdan", "Kozak", "30/08/1999");
        family.addChild(child1);

        assertEquals(1, family.getChildrenInfo().size());

        assertTrue(family.getChildrenInfo().contains(child1));
    }

    @org.junit.jupiter.api.Test
    void getPetInfo() throws ParseException {
        ArrayList<Pet> pets = new ArrayList<>();
        Pet pet1 = new DomesticCat("Patron");
        pets.add(pet1);
        Pet pet2 = new Dog("Patron");
        Human mother1 = new Human("Katya", "Kozak", "19/07/1979");
        Human father1 = new Human("Oleg", "Kozak", "04/10/1973", 90, pet1);

        Family family = new Family(mother1, father1);

        assertEquals(pets, family.getPetInfo());
//        assertEquals(pet2, family.getPetInfo());
    }

    @org.junit.jupiter.api.Test
    void addChild() throws ParseException {
        Human child1 = new Human("Vladyslav", "Kozak", "19/10/2001");
        Human child2 = new Human("Alyona", "Kozak", "12/04/2001");
        Family family = new Family(mother1, father1);
        assertTrue(family.addChild(child1));
        assertEquals(1, family.getChildrenInfo().size());
        assertEquals(child1, family.getChildrenInfo().get(0));

        assertTrue(family.addChild(child2));
        assertEquals(2, family.getChildrenInfo().size());
        assertEquals(child2, family.getChildrenInfo().get(1));
    }

    @org.junit.jupiter.api.Test
    void testDeleteChildByHumanClass() throws ParseException {
        Human child1 = new Human("Vladyslav", "Kozak", "19/10/2001");
        Human child2 = new Human("Alyona", "Kozak", "12/04/2001");
        Family family = new Family(mother1, father1);
        family.addChild(child1);
        family.addChild(child2);

        assertTrue(family.deleteChild(child2));
//        Перевіряємо, що в списку чілдренів сім'ї дійсно немає видаленого чайлда
        assertFalse(Arrays.asList(family.getChildrenInfo()).contains(child2));
//        assertFalse(Arrays.asList(family.getChildrenInfo()).contains(child1));

//        Створюємо людину, якої немає в чілдренах сім'ї та намагаємося її видалити
        Human child3 = new Human("Nastya", "Saluki", "22/09/1981");
        assertFalse(family.deleteChild(child3));
        assertEquals(1, family.getChildrenInfo().size());
    }
    @org.junit.jupiter.api.Test
    void testDeleteChildByIndex() throws ParseException {
        Human child1 = new Human("Vladyslav", "Kozak", "19/10/2001");
        Human child2 = new Human("Alyona", "Kozak", "12/04/2001");
        Family family = new Family(mother1, father1);
        family.addChild(child1);
        family.addChild(child2);

        assertTrue(family.deleteChild(0));
        assertFalse(Arrays.asList(family.getChildrenInfo()).contains(child1));

        assertFalse(family.deleteChild(2));
        assertEquals(1, family.getChildrenInfo().size());
    }

    @org.junit.jupiter.api.Test
    void testCountFamily() throws ParseException {
        Human child1 = new Human("Vladyslav", "Kozak", "19/10/2001");
        Human child2 = new Human("Alyona", "Kozak", "12/04/2001");
        Family family = new Family(mother1, father1);
        assertEquals(2, family.countFamily());
        family.addChild(child1);
        assertEquals(3, family.countFamily());
        family.addChild(child2);

        assertEquals(4, family.countFamily());
    }

    @org.junit.jupiter.api.Test
    void testToString() {
        assertEquals("Family{mother=\"Katya Kozak\", father=\"Oleg Kozak\", children=[], pet=No data for pet}",
                family.toString());
//        assertEquals("family{mother=\"Katya Kozak\", father=\"Oleg Kozak\", children=[], pet=No data for pet}",
//                family.toString());
    }
}