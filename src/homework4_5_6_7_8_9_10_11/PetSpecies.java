package homework4_5_6_7_8_9_10_11;

public enum PetSpecies {
    UNKNOWN("Unknown"),
    ROBOCAT("Robocat"),
    CAT("Cat"),
    DOG("Dog"),
    HAMSTER("Hamster"),
    FISHES("Fishes"),
    PARROT("Parrot"),
    RODENT("Rodent"),
    LIZARD("Lizard");
    private String petSpecies;
    PetSpecies(String specie){
        this.petSpecies = specie;
    }

    public String getPetSpecies(){
        return petSpecies;
    }
}