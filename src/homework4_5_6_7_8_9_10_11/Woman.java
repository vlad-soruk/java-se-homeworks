package homework4_5_6_7_8_9_10_11;

import java.text.ParseException;
import java.util.Map;

public final class Woman extends Human {
    public Woman(String name, String surname, String birthDate) throws ParseException {
        super(name, surname, birthDate);
    }

    public Woman(String name, String surname, String birthDate, Human mother, Human father) throws ParseException {
        super(name, surname, birthDate, mother, father);
    }
    public Woman(String name, String surname, String birthDate, int iq, Pet pet) throws ParseException {
        super(name, surname, birthDate, iq, pet);
    }

    public Woman(String name, String surname, String birthDate, int iq, Pet pet, Human mother, Human father, Map<String, String> schedule) throws ParseException {
        super(name, surname, birthDate, iq, pet, mother, father, schedule);
    }

    public Woman() {
        super();
    }

    @Override
    void greetPet() {
        for ( Pet pet : super.getPets() ) {
            System.out.printf("\nHello dear sweety %s!", pet.getPetName());
        }
    }

    public void makeup(){
        System.out.println("\nI`m making my makeup...");
    }
    public void takeChildrenToKindergarten(){
        System.out.println("\nI`m taking my children to kindergarten...");
    }
    public void takeChildrenToSchool(){
        System.out.println("\nI`m taking my children to school...");
    }
}
