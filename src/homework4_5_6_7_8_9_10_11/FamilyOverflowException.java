package homework4_5_6_7_8_9_10_11;

// Виняток FamilyOverflowException, успадкований від RuntimeException.
// Кидайте його у випадку якщо розмір сім'ї більший за[будь-яку бажану кількість] осіб
// і сім'я намагається народити/усиновити дитину.
public class FamilyOverflowException extends RuntimeException {
    private static String errorMessage = "Досягнуто ліміту кількості дітей у сім'ї - 5 людей!";
    public FamilyOverflowException(){
        super(errorMessage);
    }
}
