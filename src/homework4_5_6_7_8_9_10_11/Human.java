package homework4_5_6_7_8_9_10_11;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.*;

public class Human {
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private ArrayList<Pet> pets = new ArrayList<>();
    private Human mother;
    private Human father;
    private Family family;
//    Розклад позаробочих занять (schedule) (2-мірний масив: [день тижня] x [тип секції/відпочинку])
    private Map<String, String> schedule;

    public Human(String name, String surname, String birthDate) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.birthDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(birthDate).getTime();;
    }

    public Human(String name, String surname, String birthDate, Human mother, Human father) throws ParseException {
        this.name = name;
        this.surname = surname;
//        this.birthDate = LocalDate.parse(year, DateTimeFormatter.ofPattern("dd/MM/yyyy")).atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();
        this.birthDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(birthDate).getTime();
        this.mother = mother;
        this.father = father;
    }
    public Human(String name, String surname, String birthDate, int iq, Pet pet) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.birthDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(birthDate).getTime();
        this.iq = iq;
        this.pets.add(pet);
    }

    public Human(String name, String surname, String birthDate, int iq, Pet pet, Human mother, Human father, Map<String, String> schedule) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.birthDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(birthDate).getTime();
        this.iq = iq;
        this.pets.add(pet);
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }

    public Human(String name, String surname, String birthDate, int iq, Map<String, String> schedule, Pet pet) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.birthDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(birthDate).getTime();;
        this.iq = iq;
        this.schedule = schedule;
        this.pets.add(pet);
    }


    //    Конструктор для усиновлених дітей
    public Human(String name, String surname, String birthDate, int iq) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.birthDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(birthDate).getTime();;
        this.iq = iq;
    }

    public Human() {

    }
    public String getName(){
        if (name==null){
            return "\"" + "Undefined name" + "\"";
        }
        return "\"" + name + " " + surname + "\"";

    }

    public int getYear() {
        String formattedBirthDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date(birthDate));
        LocalDate parsedBirthDate = LocalDate.parse(formattedBirthDate);
        LocalDate now = LocalDate.now();

//        Returns how old is a human (quantity of years)
        return now.getYear() - parsedBirthDate.getYear();
    }

    void greetPet(){
        for (Pet pet : pets) {
            System.out.printf("\nHello, %s!", pet.getPetName());
        }
    }

    void describePet(){
        for (Pet pet : pets) {
            System.out.printf("\nI have a %s, it is %d years old, it is %s.", pet.getPetSpecies(), pet.getPetAge(), pet.getPetTrickLevel());
        }
    }

    public String describeAge(){
        LocalDate now = LocalDate.now();
        // Отримуємо дату народження людини в форматі yyyy-MM-dd з таймштампу
        String dateOfBirth = new SimpleDateFormat("yyyy-MM-dd").format( new Date(birthDate) );
        LocalDate parsedDateOfBirth = LocalDate.parse(dateOfBirth);
        Period between = Period.between(parsedDateOfBirth, now);
        int yearsOfLiving = between.getYears();
        int monthsOfLiving = between.getMonths();
        int daysOfLiving = between.getDays();

        return String.format("Years of living of the person: %d, months: %d, days: %d.", yearsOfLiving, monthsOfLiving, daysOfLiving);
    }

    public ArrayList<Pet> getPets(){
        return pets;
    }

    public int getIq() {
        return iq;
    }

    public String getSurname() {
        return surname;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("\nHuman{");
        sb.append("name='").append(name).append('\'');
        sb.append(", surname='").append(surname).append('\'');
        sb.append(", date of birth=\'").append(new SimpleDateFormat("dd/MM/yyyy").format(new Date(birthDate))).append("\'");
        if(iq == 0) {
            sb.append(", iq=Undefined");
        }
        else {
        sb.append(", iq=").append(iq);
        }
        if(pets.isEmpty()) {
            sb.append(", pet=").append("Undefined pet");
        }
        else {
            for (Pet pet : pets) {
                if (pet == null) {
                    sb.append(", pet=").append("Undefined pet");
                }
                else {
                    sb.append(", pet=").append(pet.getPetName());
                }
            }
        }
        if(mother == null) {
            sb.append(", mother=").append("Undefined");
        }
        else {
            sb.append(", mother=").append(mother.getName());
        }
        if(father == null) {
            sb.append(", father=").append("Undefined");
        }
        else {
            sb.append(", father=").append(father.getName());
        }

        if(schedule == null || schedule.isEmpty()) {
            sb.append(", schedule=No data for schedule");
        }
        else {
            sb.append(", schedule=").append(schedule);
//            sb.append(", schedule=[");
//            for(int i = 0; i < schedule.length; i++){
//                sb.append("[")
//                .append(schedule[i][0])
//                .append(", ")
//                .append(schedule[i][1])
//                .append("]");
//                if(i <  schedule.length - 1) {
//                    sb.append(", ");
//                }
//            }
//            sb.append(']');
//            sb.append('}');
        }
        return sb.toString();
    }

    public String prettyFormat() {
        final StringBuilder sb = new StringBuilder();
        sb.append("{name='").append(name).append('\'');
        sb.append(", surname='").append(surname).append('\'');
        sb.append(", date of birth=\'").append(new SimpleDateFormat("dd/MM/yyyy").format(new Date(birthDate))).append("\'");
        if(iq == 0) {
            sb.append(", iq=Undefined");
        }
        else {
            sb.append(", iq=").append(iq);
        }

//        if(pets.isEmpty()) {
//            sb.append(", pet=").append("Undefined pet");
//        }
//        else {
//            for (Pet pet : pets) {
//                sb.append(", pet=").append(pet.getPetName());
//            }
//        }

        if(schedule == null || schedule.isEmpty()) {
            sb.append(", schedule=Undefined");
        }
        else {
            sb.append(", schedule=").append(schedule);
        }

        sb.append("}");

        return sb.toString();
    }
    public Family getFamily(){
        return family;
    }
    public void setFamily(Family family){
        this.family = family;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate && iq == human.iq && Objects.equals(name, human.name) && Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, iq);
    }
}
