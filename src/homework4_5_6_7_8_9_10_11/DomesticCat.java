package homework4_5_6_7_8_9_10_11;

import java.util.Set;

public class DomesticCat extends Pet implements Foul{
    public DomesticCat(){
        super();
        super.setPetSpecies(PetSpecies.CAT);
    }
    public DomesticCat(String nickname){
        super(nickname);
        super.setPetSpecies(PetSpecies.CAT);
    }
    public DomesticCat(String nickname, int age, int tricklevel, Set<String> habits){
        super(nickname, age, tricklevel, habits);
        super.setPetSpecies(PetSpecies.CAT);
    }

    @Override
    void respond() {
        if (super.getPetName() == null) {
            System.out.println("\nHello owner! I missed you");
        }
        else {
            System.out.printf("\nHello owner. I`m %s. I missed you!", super.getPetName());
        }
    }

    @Override
    public void foul() {
        System.out.println("I need to cover up traces...");
    }
}
