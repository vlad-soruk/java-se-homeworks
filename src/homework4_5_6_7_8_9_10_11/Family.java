package homework4_5_6_7_8_9_10_11;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Family implements HumanCreator{
    private final String[] boyNames = {"Liam", "Noah", "Oliver", "James", "Elijah", "William", "Henry", "Mateo", "Theodore", "Benjamin", "Lucas",};
    private final String[] girlNames = {"Olivia", "Emma", "Charlotte", "Amelia", "Sophia", "Isabella", "Ava", "Mia", "Evelyn", "Luna", "Harper",};
    private Human mother;
    private Human father;
    private ArrayList<Human> children;
    private ArrayList<Pet> pets = new ArrayList<>();

    public Human getMotherInfo(){
        return mother;
    }
    public void setMother(Human mother) {
        this.mother = mother;
    }
    public Human getFatherInfo(){
        return father;
    }
    public void setFather(Human father) {
        this.father = father;
    }
    public ArrayList<Human> getChildrenInfo(){
        return children;
    }

    public void setChildren(ArrayList<Human> children) {
        this.children = children;
    }

    public ArrayList<Pet> getPetInfo(){
        return pets;
    }
    public void setPet(ArrayList<Pet> pet) {
        this.pets = pet;
    }

    public Family(Human mother, Human father){
        this.mother = mother;
        this.father = father;
        this.children = new ArrayList<>();
//        У батьків має встановлюватися посилання на поточну нову сім'ю
        this.mother.setFamily(this);
        this.father.setFamily(this);

        if (mother.getPets() != null) {
            this.pets.addAll(mother.getPets());
        }

        if (father.getPets() != null) {
            this.pets.addAll(father.getPets());
        }
//        this.pets = (mother.getPets()!=null) ? mother.getPets() : (father.getPets()!=null) ? father.getPets() : null;
    }

    public boolean addPet(Pet pet) {
        pets.add(pet);
        return true;
    }

    public boolean addChild(Human child){
        children.add(child);
//        Human[] updatedChildrenArray = new Human[children.length + 1];
//        Копіюємо все, що знаходиться в children в updatedChildrenArray
//        System.arraycopy(children, 0, updatedChildrenArray, 0, children.length);
//        updatedChildrenArray[children.length] = child;
//        child.setFamily(this);
//        children = updatedChildrenArray;
        return true;
    }
    public boolean deleteChild(int index) {
        if (index < 0 || index >= children.size())
            return false;

//        Human[] updatedChildrenArray = new Human[children.length - 1];
        for (int i = 0, j = 0; i < children.size(); i++) {
            if (i == index) {
                children.remove(index);
//                updatedChildrenArray[j++] = children[i];
            }
        }

        children.get(index).setFamily(null);
//        children = updatedChildrenArray;
        return true;
    }

    public boolean deleteChild(Human child) {
        for (int i = 0; i < children.size(); i++) {
//            Наприклад, children.length = 4, а хочемо видалити 3-ій елемент, що
//            знаходиться в children[2];
            if (children.get(i).equals(child)) {
//                Зменшуємо довжину масива на одиницю
//                Human[] updatedChildrenArray = new Human[children.length - 1];
//                Копіюємо з масиву children в масив updatedChildrenArray
//                усе, до елемента, що видаляємо. У нашому випадку копіюємо 2
//                елемента - children[0] та children[1]
//                System.arraycopy(children, 0, updatedChildrenArray, 0, i);
//                Далі копіюємо, все, що залишилося й знаходиться після елемента, що
//                видаляємо, тобто після children[2]
//                System.arraycopy(children, i + 1, updatedChildrenArray, i, children.length - i - 1);
//                children = updatedChildrenArray;
                children.remove(child);
                child.setFamily(null);
                return true;
            }
        }
        return false;
    }

    public int countFamily(){
        return 2 + children.size();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Family{");
        sb.append("mother=").append(mother.getName());
        sb.append(", father=").append(father.getName());
        sb.append(", children=").append(children.toString());
        sb.append(", pet=");

        if (pets == null || pets.isEmpty()) {
            sb.append("No data for pet");
        }
        else {
            for (Pet pet : pets) {
                sb.append(pet.getPetName());
            }
        }

        sb.append('}');
        return sb.toString();
    }

    public String prettyFormat() {
        final StringBuilder sb = new StringBuilder("family:\n");
        sb.append("\tmother: ").append(mother.prettyFormat()).append(",\n");
        sb.append("\tfather: ").append(father.prettyFormat()).append(",\n");
        sb.append("\tchildren:\n");
        if (!children.isEmpty()) {
            for (Human child : children) {
                sb.append("\t\t\t")
                        .append(children.indexOf(child) + 1)
                        .append(": ")
                        .append(child.prettyFormat())
                        .append("\n");
            }
        }
        else {
            sb.append("\t\t\tNo children\n");
        }
        sb.append("\tpets: ");

        if (pets.isEmpty()) {
            sb.append("No data for pet");
        }
        else {
            sb.append(pets);
        }

        return sb.toString();
    }

    @Override
    public Human bornChild() throws ParseException {
        Random random = new Random();
        String babyName;
        int iq = (mother.getIq() + father.getIq())/2;
        boolean isMale = random.nextBoolean();
        String surname = father.getSurname();

        String birthDate = LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy", Locale.ENGLISH));

        if(isMale) {
            babyName = boyNames[random.nextInt(boyNames.length)];
            Man newbornBaby = new Man(babyName, surname, birthDate, iq, null, mother, father, null);
            addChild(newbornBaby);
            return newbornBaby;
        }
        else {
            babyName = girlNames[random.nextInt(girlNames.length)];
            Woman newbornBaby = new Woman(babyName, surname, birthDate, iq, null, mother, father, null);
            addChild(newbornBaby);
            return newbornBaby;
        }
    }
    public Human bornChild(String boyName, String girlName) throws ParseException {
        Random random = new Random();
        int iq = (mother.getIq() + father.getIq())/2;
        boolean isMale = random.nextBoolean();
        String surname = father.getSurname();

        String birthDate = LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

        if(isMale) {
            Pet pet = mother.getPets().isEmpty() ? father.getPets().get(0) : mother.getPets().get(0);
            Man newbornBaby = new Man(boyName, surname, birthDate, iq, pet, mother, father, new HashMap<>());
            addChild(newbornBaby);
            return newbornBaby;
        }
        else {
            Pet pet = mother.getPets().isEmpty() ? father.getPets().get(0) : mother.getPets().get(0);
            Woman newbornBaby = new Woman(girlName, surname, birthDate, iq, pet, mother, father, new HashMap<>());
            addChild(newbornBaby);
            return newbornBaby;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father) && Objects.equals(children, family.children) && Objects.equals(pets, family.pets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children, pets);
    }
}