package homework4_5_6_7_8_9_10_11;

public enum DayOfWeek {
    MONDAY("Monday"),
    TUESDAY("Tuesday"),
    WEDNESDAY("Wednesday"),
    THURSDAY("Thursday"),
    FRIDAY("Friday"),
    SATURDAY("Saturday"),
    SUNDAY("Sunday");

    private String day;

    DayOfWeek(String someDay) {
        this.day = someDay;
    }

    public String getDayOfWeek() {
        return day;
    }
}
