package homework1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class RandomNumber {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter your name...");

        String userName = sc.nextLine();
        System.out.println("Let the game begin!");

        Random rand = new Random();
        int randomNumber = rand.nextInt(101);
//        System.out.println(randomNumber);

        // Викликаємо метод, що перевірятиме валідність введених даних
        // та поверне число
        int userNumber = checkEnteredNumber(sc);

        ArrayList<Integer> listOfUserNumbers = new ArrayList<>();

        while(true){
            // Додаємо число, введене користувачем, в список, який потім будемо
            // сортувати та виводити
            listOfUserNumbers.add(userNumber);

            if(userNumber < randomNumber) {
                System.out.println("Your number is too small. Please, try again");
                userNumber = checkEnteredNumber(sc);
            }
            else if(userNumber > randomNumber) {
                System.out.println("Your number is too big. Please, try again");
                userNumber = checkEnteredNumber(sc);
            }
            // Функціонал, якщо користувач вгадав число
            else {
                System.out.printf("Congratulations, %s!\n", userName);
                // Сортуємо список чисел, введених користувачем
                Collections.sort(listOfUserNumbers);
                System.out.printf("Your sorted entered numbers: %s\n", listOfUserNumbers);
                break;
            }
        }
    }

    // Метод для перевірки введеного значення користувачем
    public static int checkEnteredNumber(Scanner sc){
        int userNumber;

        while(true) {
            System.out.println("Enter your number...");

            try{
                userNumber = sc.nextInt();
                break;
            }
            catch(Exception ex) {
                sc.nextLine();
                System.out.println("You entered not a number!");
            }
        }

        return userNumber;
    }
}